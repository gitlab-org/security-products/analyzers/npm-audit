package main

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/npm-audit/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/gitlab/security-products/tests")
}

func TestConvertNpm(t *testing.T) {
	prependPath := "app"
	category := report.CategoryDependencyScanning
	scanner := report.Scanner{
		ID:   metadata.AnalyzerID,
		Name: metadata.ScannerName,
	}

	var tcs = []struct {
		name                   string
		reportNodeModulesPaths bool
		input                  string
		want                   *report.Report
	}{

		{
			name:                   "report node_modules paths",
			reportNodeModulesPaths: true,
			input: `
{
  "actions": [
    {
      "isMajor": false,
      "action": "install",
      "resolves": [
        {
          "id": 1166,
          "path": "@commercial/subtext",
          "dev": false,
          "optional": false,
          "bundled": false
        },
        {
          "id": 1484,
          "path": "@commercial/subtext",
          "dev": false,
          "optional": false,
          "bundled": false
        }
      ],
      "module": "@commercial/subtext",
      "target": "5.1.2"
    }
  ],
  "advisories": {
    "1166": {
      "findings": [
        {
          "version": "5.1.0",
          "paths": [
            "@commercial/subtext"
          ]
        }
      ],
      "id": 1166,
      "created": "2019-09-16T17:35:46.904Z",
      "updated": "2019-09-16T17:36:06.721Z",
      "deleted": null,
      "title": "Denial of Service",
      "found_by": {
        "link": "https://github.com/chuhaienko",
        "name": "Yurii Chuhaienko"
      },
      "reported_by": {
        "link": "https://github.com/chuhaienko",
        "name": "Yurii Chuhaienko"
      },
      "module_name": "@commercial/subtext",
      "cves": [],
      "vulnerable_versions": "<5.1.1",
      "patched_versions": ">=5.1.1",
      "overview": "Overview.",
      "recommendation": "Upgrade to version 5.1.1 or later.",
      "references": "- [GitHub issue](https://github.com/hapijs/subtext/issues/72)",
      "access": "public",
      "severity": "high",
      "cwe": "CWE-400",
      "metadata": {
        "module_type": "",
        "exploitability": 5,
        "affected_components": ""
      },
      "url": "https://npmjs.com/advisories/1166"
    }
  },
  "muted": [],
  "metadata": {
    "vulnerabilities": {
      "info": 0,
      "low": 0,
      "moderate": 0,
      "high": 2,
      "critical": 0
    },
    "dependencies": 24,
    "devDependencies": 0,
    "optionalDependencies": 0,
    "totalDependencies": 24
  },
  "runId": "a0efafa1-d1b8-4e65-9b11-0b71bd58f0da"
}`,
			want: &report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:   category,
						Scanner:    scanner,
						Name:       "Denial of Service",
						Message:    "Denial of Service in @commercial/subtext",
						CompareKey: "app/package.json:@commercial/subtext:npm-audit:1166",
						Severity:   report.SeverityLevelHigh,
						Location: report.Location{
							File: "app/package.json",
							Dependency: &report.Dependency{
								Package: report.Package{
									Name: "@commercial/subtext",
								},
								Version: "5.1.0",
							},
						},
						Identifiers: []report.Identifier{
							{
								Type:  "npm-audit",
								Name:  "NPM-1166",
								Value: "1166",
								URL:   "https://npmjs.com/advisories/1166",
							},
						},
						Links: []report.Link{
							{
								URL: "https://npmjs.com/advisories/1166",
							},
						},
					},
				},
				DependencyFiles: []report.DependencyFile{
					{
						Path:           "app/package.json",
						PackageManager: "npm",
						Dependencies: []report.Dependency{
							{
								Package: report.Package{
									Name: "@commercial/subtext",
								},
								Version: "5.1.0",
							},
						},
					},
				},
				Scan: report.Scan{
					Type: "",
				},
			},
		},
	}

	pm := npmPackageManager{}
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {

			if tc.reportNodeModulesPaths {
				os.Setenv("REPORT_NODE_MODULES_PATHS", "")
			} else {
				os.Unsetenv("REPORT_NODE_MODULES_PATHS")
			}

			r := strings.NewReader(tc.input)
			got, err := convertReport(pm, r, prependPath)
			require.NoError(t, err)
			require.Equal(t, tc.want, got)
		})
	}
}
