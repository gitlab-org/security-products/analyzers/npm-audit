package main

import (
	"encoding/json"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	issue "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

type npmPackageManager struct{}

func (pm npmPackageManager) name() issue.PackageManager {
	return issue.PackageManagerNpm
}

func (pm npmPackageManager) build(path string) error {
	cmd := exec.Command("npm", "install")

	// if a lockfile is present foce npm to use it
	if pm.lockfilePresent(path) {
		log.Info("lockfile detected: use npm ci")
		cmd = exec.Command("npm", "ci")
	}

	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (pm npmPackageManager) analyze(path string) ([]byte, error) {
	cmd := exec.Command("npm", "audit", "--json")
	cmd.Dir = path
	cmd.Env = os.Environ()
	return cmd.Output()
}

func (pm npmPackageManager) lockfilePresent(path string) bool {
	return pathExists(filepath.Join(path, pm.lockfile()))
}

func (pm npmPackageManager) prepare(reader io.Reader) (*AuditReport, error) {
	// decode output
	var result AuditReport
	err := json.NewDecoder(reader).Decode(&result)
	return &result, err
}

func (pm npmPackageManager) lockfile() string {
	return npmLockfile
}
